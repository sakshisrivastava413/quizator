import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { QuizlistPage } from '../pages/quizlist/quizlist';
import { QuizPage } from '../pages/quiz/quiz';
import { SummaryPage } from '../pages/summary/summary';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';

const firebase_config = {
  apiKey: "AIzaSyCfcVM6g4Z3pX1vvZsgjX4mUrvnh3BhqAw",
  authDomain: "quizator-3555f.firebaseapp.com",
  databaseURL: "https://quizator-3555f.firebaseio.com",
  projectId: "quizator-3555f",
  storageBucket: "",
  messagingSenderId: "1090928561134"
};

@NgModule({
  declarations: [
    MyApp,
    QuizlistPage,
    QuizPage,
    SummaryPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebase_config),
    AngularFireDatabaseModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    QuizlistPage,
    QuizPage,
    SummaryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

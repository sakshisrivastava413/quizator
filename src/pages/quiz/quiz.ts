import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { SummaryPage } from '../summary/summary';

@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html',
})
export class QuizPage {

  quiz: any;
  selections: boolean[][];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alert: AlertController, public loading: LoadingController) {
    this.quiz = this.navParams.data['quiz'];
    console.log(this.quiz);
    this.selections = [];
    for (let i = 0; i < this.quiz.questions.length; i++) {
      this.selections.push([]);
      for (let j = 0; j < this.quiz.answers[i].options.length; j++) {
        this.selections[i].push(false);
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizPage');
  }

  select(qi, j) {
    this.selections[qi][j] = !this.selections[qi][j];
    let cnt = 0;
    this.selections[qi].forEach(item => {
      if (item) {
        cnt++;
        if (cnt >= 3) {
          // show toast
          this.alert.create({
            title: 'error',
            message: 'cannot select more than 2',
            buttons: ['ok']
          }).present();
          this.selections[qi][j] = !this.selections[qi][j];
        }
      }
    });
  }

  submit() {
    let l = this.loading.create({
      content: 'wait... preparing results...',
      spinner: 'dots',
      dismissOnPageChange: true
    });
    l.present();

    let totalCorrect = 0;
    this.quiz.questions.forEach((q, i) => {
      if (this.selections[i][this.quiz.answers[i].correctOption]) {
        totalCorrect++;
      }
    });

    setTimeout(() => {
      // redirect user to summarypage 
      this.navCtrl.setRoot(SummaryPage, {
        totalCorrect: totalCorrect,
        totalQuestions: this.quiz.questions.length,
        quiz: this.quiz,
        selections: this.selections
      });
    }, 1500);

  }

}
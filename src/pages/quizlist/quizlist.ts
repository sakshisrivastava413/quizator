import { QuizPage } from './../quiz/quiz';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@Component({
  selector: 'page-quizlist',
  templateUrl: 'quizlist.html',
})
export class QuizlistPage {

  quizes: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: AngularFireDatabase, public loading: LoadingController) {
    let l = this.loading.create({
      content: 'loading...'
    });
    l.present();
    this.db.list(`quizes`).valueChanges().subscribe(quizes => {
      this.quizes = quizes;
      l.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizlistPage');
  }

  quizPage(quiz) {
    this.navCtrl.push(QuizPage, {
      quiz: quiz
    });
  }

}

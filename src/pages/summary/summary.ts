import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { QuizlistPage } from '../quizlist/quizlist';

@Component({
  selector: 'page-summary',
  templateUrl: 'summary.html',
})
export class SummaryPage {

  quiz: any;
  selections: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.quiz = this.navParams.data['quiz'];
    this.selections = this.navParams.data['selections'];
    console.log(this.navParams.data);
  }

  GoHome() {
    this.navCtrl.setRoot(QuizlistPage);
  }  

}
